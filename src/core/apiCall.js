import axios from "axios";

const baseURL = "https://one-pass-mevn.herokuapp.com/";
//const baseURL = "http://localhost:3000/";

export default axios.create({
  baseURL
});
