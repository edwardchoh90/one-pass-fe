import Vue from "vue";
import Router from "vue-router";

//apps
import Dashboard from "../views/apps/Dashboard.vue";
import Holiday from "../views/apps/Leave/Holiday.vue";
import LeaveList from "../views/apps/Leave/LeaveList.vue";
import Users from "../views/apps/Users/Users.vue";
import Devices from "../views/apps/Devices/Devices.vue";
import Attendance from "../views/apps/Attendance/Attendance.vue";

//pages
import Login from "../views/pages/authentication/Login.vue";
import Register from "../views/pages/authentication/Register.vue";
import ForgotPassword from "../views/pages/authentication/ForgotPassword.vue";
import Profile from "../views/pages/Profile.vue";
import NotFound from "../views/pages/NotFound.vue";
import store from "../store";
import layouts from "../layout";
import Schedule from "../views/apps/Schedule/Schedule.vue";
import Summary from "../views/apps/Attendance/Summary.vue";

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      alias: "/dashboard",
      name: "dashboard",
      component: Dashboard,
      meta: {
        auth: true,
        layout: layouts.navTop,
        searchable: true,
        tags: ["app"]
      }
    },
    {
      path: "/attendance",
      name: "Attendance",
      component: Attendance,
      meta: {
        auth: true,
        searchable: true,
        layout: layouts.navTop,
        tags: ["app"]
      }
    },
    {
      path: "/summary",
      name: "Summary",
      component: Summary,
      meta: {
        auth: true,
        searchable: true,
        layout: layouts.navTop,
        tags: ["app"]
      }
    },
    {
      path: "/schedule",
      name: "Schedule",
      component: Schedule,
      meta: {
        auth: true,
        searchable: true,
        layout: layouts.navTop,
        tags: ["app"]
      }
    },
    {
      path: "/holiday",
      name: "Holiday",
      component: Holiday,
      meta: {
        auth: true,
        searchable: true,
        layout: layouts.navTop,
        tags: ["app"]
      }
    },
    {
      path: "/devices",
      name: "Devices",
      component: Devices,
      meta: {
        auth: true,
        searchable: true,
        layout: layouts.navTop,
        tags: ["app"]
      }
    },
    {
      path: "/leave",
      name: "LeaveList",
      component: LeaveList,
      meta: {
        auth: true,
        searchable: true,
        layout: layouts.navTop,
        tags: ["app"]
      }
    },
    {
      path: "/users",
      name: "Users",
      component: Users,
      meta: {
        auth: true,
        searchable: true,
        tags: ["app"]
      }
    },

    {
      path: "/login",
      name: "login",
      component: Login,
      meta: {
        layout: layouts.contenOnly
      }
    },
    {
      path: "/register",
      name: "register",
      component: Register,
      meta: {
        layout: layouts.contenOnly
      }
    },
    {
      path: "/forgot-password",
      name: "forgot-password",
      component: ForgotPassword,
      meta: {
        layout: layouts.contenOnly
      }
    },
    {
      path: "/logout",
      beforeEnter(to, from, next) {
        auth.logout();
        next({
          path: "/login"
        });
      }
    },
    {
      path: "*",
      name: "not-found",
      component: NotFound,
      meta: {
        layout: layouts.contenOnly
      }
    }
  ]
});

const l = {
  contenOnly() {
    store.commit("setLayout", layouts.contenOnly);
  },
  navLeft() {
    store.commit("setLayout", layouts.navLeft);
  },
  navRight() {
    store.commit("setLayout", layouts.navRight);
  },
  navTop() {
    store.commit("setLayout", layouts.navTop);
  },
  navBottom() {
    store.commit("setLayout", layouts.navBottom);
  },
  set(layout) {
    store.commit("setLayout", layout);
  }
};

//insert here login logic
const auth = {
  loggedIn() {
    return store.getters.isLogged;
  },
  logout() {
    store.commit("setLogout");
  }
};

router.beforeEach((to, from, next) => {
  let authrequired = false;
  if (to && to.meta && to.meta.auth) authrequired = true;

  //console.log('authrequired', authrequired, to.name)

  if (authrequired) {
    if (auth.loggedIn()) {
      if (to.name === "login") {
        window.location.href = "/";
        return false;
      } else {
        next();
      }
    } else {
      if (to.name !== "login") {
        window.location.href = "/login";
        return false;
      }
      next();
    }
  } else {
    if (auth.loggedIn() && to.name === "login") {
      window.location.href = "/";
      return false;
    } else {
      next();
    }
  }

  if (to && to.meta && to.meta.layout) {
    l.set(to.meta.layout);
  }
});

router.afterEach((to, from) => {
  setTimeout(() => {
    store.commit("setSplashScreen", false);
  }, 500);
});

export default router;
